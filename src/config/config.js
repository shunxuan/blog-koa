module.exports = {
  port: process.env.PORT || 3000,
  session: {
    key: 'blogNode',
    maxAge: 86400000
  },
  // mongodb: 'mongodb://localhost:27017/blogNode',
  // connect:'mongodb+srv://wjh:wangjiahui0915@zhihu-g8fyq.mongodb.net/test?retryWrites=true&w=majority',
  mongodb: 'mongodb+srv://wjh:wangjiahui0915@zhihu-g8fyq.mongodb.net/blog?retryWrites=true&w=majority',
  TOKEN: {
    secret: 'firepage-blog', // secret is very important!
    expiresIn: '720h' // token 有效期
  }
};
