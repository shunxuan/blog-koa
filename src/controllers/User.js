const crypto = require('crypto');
const User = require('../models/User')
const { createToken } = require('../common/Token')
const Joi = require('joi')

const userHelper = {
  MD5_SUFFIX: 'www.firepage.xyz*&^%$#',
  md5(pwd) {
    return crypto.createHash('md5').update(pwd).digest('hex');
  }
}

class UserController {
  // 站内用户登录
  async login(ctx) {
    const validator = ctx.validate(ctx.request.body, {
      email: Joi.string().required(),
      password: Joi.string().required()
    })
    if (validator) {
      const {
        email,
        password
      } = ctx.request.body
      const user = await User.findOne({
        email,
        password: userHelper.md5(password + userHelper.MD5_SUFFIX)
      })
      if (!user) {
        ctx.throw(404, '用户不存在')
      } else {
        // const { id, role } = user
        // const token = createToken({ username: user.username, userId: id, role }) // 生成 token
        ctx.session.userInfo = user
        ctx.body = {
          code: 0,
          data: user,
          message: '登录成功'
          // token
        }
      }
    }
  }

  // 站内用户登录
  // https://blog.csdn.net/qq_38734862/article/details/107715579
  // https://editor.swagger.io/?_ga=2.132017415.928281264.1596593063-247237175.1594971087
  /**
   * @swagger
   * /loginAdmin: # 接口地址
   *   post: # 请求体
   *     summary: "用户登入"
   *     description: 用户登入 # 接口信息
   *     tags: [用户模块] # 模块名称
   *     produces:
   *       - application/x-www-form-urlencoded # 响应内容类型
   *     parameters: # 请求参数
   *       - name: email
   *         description: 邮箱
   *         in: formData # 参数的位置，可能的值有 "query", "header", "path" 或 "cookie" 没有formData，但是我加了不报错
   *         required: true
   *         type: string
   *       - name: passowrd
   *         description: 密码
   *         in: formData
   *         required: true
   *         type: string # 可能的值有string、number、file（文件）等
   *     responses:
   *       '200':
   *         description: Ok
   *         schema: # 返回体说明
   *           type: 'object'
   *           properties:
   *             code:
   *               type: 'number'
   *             data:
   *               type: 'object'
   *               description: 返回数据
   *             message:
   *               type: 'string'
   *               description: 消息提示
   *       '400':
   *         description: 请求参数错误
   *       '404':
   *         description: not found
   */
  async loginAdmin(ctx) {
    const validator = ctx.validate(ctx.request.body, {
      email: Joi.string().required(),
      password: Joi.string().required()
    })
    const {
      email,
      password
    } = ctx.request.body
    if (validator) {
      const user = await User.findOne({
        email,
        password: userHelper.md5(password + userHelper.MD5_SUFFIX)
      })

      if (!user) {
        ctx.throw(404, '用户不存在')
      } else {
        if (user.type === 0) {
          const {
            id,
            role
          } = user
          const token = createToken({
            username: user.username,
            userId: id,
            role
          }) // 生成 token
          ctx.session.userInfo = user

          ctx.body = {
            code: 0,
            data: user,
            message: '登录成功',
            token
          }
        } else {
          ctx.throw(403, '用户无权限！')
        }
      }
    } else {
      ctx.throw(400, '参数错误！')
    }
  }

  // 后台管理中当前用户
  async currentUser(ctx) {
    const user = await ctx.session.userInfo;
    // console.log(user);
    user.avatar = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABaCAYAAAA4qEECAAAAAXNSR0IArs4c6QAABSxJREFUeF7tmtlXGkkUxj9EQUUTViFBxeASMWaZvMxfP0/zMGfmzMRMTHQwQSMxhk0WEVkUmHN7DthgIDTLpXRuPXmkq27Xr7++XferMv3y6291SBs5AZOAHjljLYCA5uEsoJk4C2gBzUWAKY7kaAHNRIApjChaQDMRYAojihbQTASYwoiiBTQTAaYwomgBzUSAKYwoWkAzEWAKI4oW0EwEmMKIogU0EwGmMKJoAc1EgCmMKFpAMxFgCiOKFtBMBJjCiKL/76DNZjP8vgW4XU7Mzkxj0mzGxMREE0u9Xke1WkPlqoLzfAHxZApnmSwTNuNhlFO01WrBamAZHpcDBNtIK5XL+PotjuOTUyPdWK5VCrRvwa1BJtiDtGzuHOHIEQqXxUGGGWpfZUC7HHZsrgU7QqZUUavVmpM3mUwtqaSdykXhEh/CH5WBrQRoShGvn29hfs7WwqtarSKdySGWSiGZSt9SmG12Bh6XE16PG/S3vtHpesrbe+FPQ1Vmv4MpATqw6MeTZX+LQum1P4gcIZM772luwcASlh77WvL61fW1NkY8edbTGKO8SAnQr7ZDcNofNudZubpC+NMRkme3VdwNxsbqCvw+LyitNNq3eAL7Hw9HybCnsZUA/fPrly2vfiabw877/Z4moL/owfwctjfXMW21Nv+dO8/jr3cfDI817A5Kgj6NJ/BPnypsvB20xgbqKBSL+PPt+2FzMzyekqApL+/s7hmejModlAD9Yusp3E5Hk9P1dRWR46hWfNyXpgToxcc+rVAxm29KbIIdSya1Kq9crtx53kqApnX089BGy8qjQZaKFFrqkY9B62KVqj0jT18J0HTDVHCE1ldBK4dujZReLJWQy+e15V8m29s62wiUUVyrDGiaHHkcaysBzVDSO3XdJl6t1VAsljTFxxJJZRWvFOgGUKfDjmX/IzycnzPk4NXqdVxcFHAYPUFaMctUSdAN4JS7ydEjP2PONoupycmWqq+T0imvJ87SWnVJfokKTWnQ7YAotRB0KtfnbTZYLFNdwaezOezuHygB+06B7gSe4NNH1KzbgaFryVrVHLyDyNhFfadB6+nRqoUcPCp89KaSKg7evQHdgL61sar506o5eGMF7XLasf4kAKvFAphMIHOTKsGj6Enfrzpt5FJJPztzsxGggoM3VtD0mm+uB2GZmmqCHYZ//NN2CA6dv03V5O9v/u774Q2j41hBk/pePtvEzPR0cy75iwLe7O4NtFJoB037h3/svBsGr77HGCtouut2546KjujJKQ6Pv/Q1qe89PFrmve1jI6GvG+jQaeyg/Y+8WFsh5+7mDEe5UkHkcxSxRMrwXNs/hoM+OMM3oCpoAvwi9BQO+4OWW9Rs0kQSkeMvPaURGmcjGIB3wYMJ3Z7hZbGE3f3w2D2QsSua6HY700EbtXTkIJlOf/fIAT0gn8cDWsHoP6o0LpXig65i7o2iGxOh3evgypLmZ3Rq7YdoyOHTr5f1/VSqCum+lFB0A5Db5dBsUvqgDdJIyaexBA4OPw8yzFD7KgWaZka5Nri8CO+C+1Yq+NHMxSb9EaEOv1MpTZsAZJFapizanqI+Vfx3dJd2XMqg6u9rLD72j16nqSqn6D6fifLdBDTTIxLQApqJAFMYUbSAZiLAFEYULaCZCDCFEUULaCYCTGFE0QKaiQBTGFG0gGYiwBRGFC2gmQgwhRFFC2gmAkxhRNECmokAUxhRtIBmIsAURhQtoJkIMIURRQtoJgJMYUTRApqJAFMYUTQT6H8BvMjTFWdhDNsAAAAASUVORK5CYII=';
    ctx.body = {
      status: 200,
      data: user,
      code: 0
    }
  }

  // 新用户注册
  async register(ctx) {
    const validator = ctx.validate(ctx.request.body, {
      name: Joi.string().required(),
      password: Joi.string().required(),
      email: Joi.string()
        .email()
        .required(),
      type: Joi.number().required()
    })

    if (validator) {
      const {
        name,
        password,
        phone,
        email,
        introduce,
        type
      } = ctx.request.body
      const result = await User.findOne({ where: { email } })
      if (result) {
        ctx.throw(403, '邮箱已被注册')
      } else {
        const user = await User.findOne({ where: { name } })
        if (user) {
          ctx.client(403, '用户名已被占用')
        } else {
          const newUser = new User({
            email,
            name,
            password: userHelper.md5(password + userHelper.MD5_SUFFIX),
            phone,
            type,
            introduce
          });
          const data = await newUser.save()
          ctx.body = {
            status: 200,
            data: data,
            code: 0
          }
        }
      }
    }
  }

  // 获取用户列表
  /**
   * @swagger
   * /getUserList: # 接口地址
   *   get: # 请求体
   *     summary: "获取用户列表"
   *     description: 获取用户列表 # 接口信息
   *     tags: [用户模块] # 模块名称
   *     produces:
   *       - application/x-www-form-urlencoded # 响应内容类型
   *     parameters: # 请求参数
   *       - name: pageNum
   *         description: 当前页
   *         in: 没有formData # 参数的位置，可能的值有 "query", "header", "path" 或 "cookie" 没有formData，但是我加了不报错
   *         type: string
   *       - name: pageSize
   *         description: 每页条数
   *         in: 没有formData
   *         type: string # 可能的值有string、number、file（文件）等
   *     responses:
   *       '200':
   *         description: Ok
   *         schema: # 返回体说明
   *           type: 'object'
   *           properties:
   *             code:
   *               type: 'number'
   *             data:
   *               type: 'object'
   *               description: 返回数据
   *             message:
   *               type: 'string'
   *               description: 消息提示
   *       '400':
   *         description: 请求参数错误
   *       '404':
   *         description: not found
   */
  async getUserList(ctx) {
    const validator = ctx.validate(ctx.query, {
      pageNum: Joi.number(),
      pageSize: Joi.number()
    })
    if (validator) {
      const {
        pageNum = 0,
        pageSize,
        keyword,
        type = ''
      } = ctx.query
      let query = {};
      if (keyword) {
        const reg = new RegExp(keyword, 'i');
        query = {
          $or: [{ name: { $regex: reg } }, { email: { $regex: reg } }]
        };
      }
      if (type !== '') {
        const { ...rest } = query
        query = {
          ...rest,
          type
        }
      }
      const item = {
        _id: 1,
        email: 1,
        name: 1,
        avatar: 1,
        phone: 1,
        introduce: 1,
        type: 1,
        create_time: 1
      }
      const options = {
        skip: pageNum - 1 < 0 ? 0 : (pageNum - 1) * pageSize,
        limit: Number(pageSize),
        sort: { create_time: -1 }
      };
      console.log('query---->>', query);
      const list = await User.find(query, item, options)
      const total = await User.count(query)
      ctx.body = {
        status: 200,
        data: {
          list,
          total
        },
        code: 0,
        message: 'success'
      }
    }
  }

  async deleteUser(ctx) {
    const validator = ctx.validate(ctx.request.body, {
      _id: Joi.string().required()
    })
    const { _id } = ctx.request.body
    if (validator) {
      const result = await User.remove({ _id })
      if (result.deletedCount) {
        ctx.body = {
          status: 200,
          message: '用户删除成功',
          data: { _id },
          code: 0
        }
      } else {
        ctx.body = {
          status: 200,
          message: '用户不存在',
          code: 1
        }
      }
    }
  }

  async updateUser(ctx) {
    const validator = ctx.validate(ctx.request.body, {
      _id: Joi.string().required(),
      name: Joi.string().required(),
      password: Joi.string().required(),
      email: Joi.string()
        .email()
        .required(),
      type: Joi.number().required()
    })
    if (validator) {
      const {
        _id,
        name,
        password,
        phone,
        email,
        introduce,
        type
      } = ctx.request.body
      const result = await User.findOneAndUpdate(
        { _id },
        {
          name,
          password,
          phone,
          email,
          introduce,
          type
        }, { new: true })
      ctx.body = {
        status: 200,
        data: result,
        message: '更新成功',
        code: 0
      }
    }
  }

  async logout(ctx) {
    if (ctx.session.userInfo) {
      ctx.session.userInfo = null
      ctx.body = {
        status: 200,
        message: '用户退出登录成功',
        code: 0
      }
    } else {
      ctx.body = {
        status: 200,
        message: '您尚未登录',
        code: 1
      }
    }
  }

  async getUserDetail(ctx) {
    const validator = ctx.validate(ctx.request.body, {
      _id: Joi.string().required()
    })
    if (validator) {
      const { _id } = ctx.request.body
      const result = await User.findOne({ _id })
      ctx.body = {
        status: 200,
        data: result,
        message: '操作成功',
        code: 0
      }
    }
  }
}

module.exports = new UserController()
